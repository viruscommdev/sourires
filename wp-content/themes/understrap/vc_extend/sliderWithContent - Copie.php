<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "slider_with_content",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'image1' => '',
            'image2' => '',
            'height' => '400px',
            'contentslider1'=>""
        ), $atts);
        ob_start();
        ?>
        <div class="carousel carousel-slider center"  style="    width:100%;">
            <div class="carousel-item red white-text" href="#one!">
                <?=wp_get_attachment_image($atts['image1'],"full") ?>
                <div class="contentSlider">
                    <?= urldecode(base64_decode($atts["contentslider1"])) ?>
                </div>
            </div>
            <div class="carousel-item amber white-text" href="#two!">
                <?=wp_get_attachment_image($atts['image2'],"full") ?>
            </div>

            <!--<div class="carousel-fixed-item center">
                <div class="btns">
                    <a class="btn-Slider left" onclick="Slider.carousel('prev')"></a>
                    <a class="btn-Slider right" onclick="Slider.carousel('next')"></a>
                </div>
            </div>-->
        </div>
        <script>

        </script>
        <?php
        return ob_get_clean();
    }
    , "slider with content",
    [
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("image 1", "my-text-domain"),
            "param_name" => "image1",
            "value" => __("0", "my-text-domain")
        ),
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("image 2", "my-text-domain"),
            "param_name" => "image2",
            "value" => __("0", "my-text-domain")
        ),
        [
            "type" => "textarea_raw_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("content Slider 1", "my-text-domain"),
            "param_name" => "contentslider1",
            "value" => __("", "my-text-domain")
        ],
    ]);