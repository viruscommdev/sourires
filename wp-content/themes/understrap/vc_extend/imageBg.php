<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "imagebg",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'image' => '',
            'style' => 'background-repeat:no-repeat;background-size:contain;',
            'class' => '',
        ), $atts);
        ob_start();
        $dataImage=wp_get_attachment_metadata($atts["image"]);
        ?>
        <div class="imagebg <?=$atts["class"]?>"
             style="background-image: url('<?= wp_get_attachment_image_url($atts['image'], "full") ?>');
                     width: <?=$dataImage["width"]?>px;
                     height: <?=$dataImage["height"]?>px;
             <?= $atts['style'] ?>">
            <?= wp_get_attachment_image($atts['image'], "full", false, ["style" => "display:none;"]) ?>
        </div>
        <?php
        return ob_get_clean();
    }
    , "image in background",
    [
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("image", "my-text-domain"),
            "param_name" => "image",
            "value" => __("0", "my-text-domain")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("style", "my-text-domain"),
            "param_name" => "style",
            "value" => __("background-repeat:no-repeat;background-size:contain;", "my-text-domain")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("class", "my-text-domain"),
            "param_name" => "class",
            "value" => __(" ", "my-text-domain")
        )
    ]);