<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "map",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'mapid' => 'map1',
            'color' => '#333',
            'icon' => '',
            'height' => '400px',
            'lat-long' => '45.422224, -75.7420064'
        ), $atts);
        ob_start();
        ?>
        <div id="<?= $atts['mapid'] ?>" style="width: 100%;height: <?= $atts['height'] ?>"
             data-pin="<?= wp_get_attachment_image_url($atts['icon'], "full") ?>"></div>
        <script>
            <?=$atts['mapid'] . "Map"?>= null;

            function <?=$atts['mapid'] . "Mapfn"?>() {
                var points = new google.maps.LatLng(<?=$atts['lat-long']?>);
                var styles = [
                    {
                        "stylers": [
                            {"visibility": "on"},
                            {"hue": "<?=$atts['color']?>"}
                        ]
                    }
                ];

                var mapOptions1 = {
                    scrollwheel: false,
                    // How zoomed in you want the map to start at (always required)
                    zoom: 14,
                    center: points, // New York
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: styles
                };
                // Create the Google Map using our element and options defined above
                <?=$atts['mapid'] . "Map"?> = new google.maps.Map(document.getElementById("<?=$atts['mapid']?>"), mapOptions1);

            }

            function reCallUntilItsWork() {
                try {
                    gMapReady(<?=$atts['mapid'] . "Mapfn"?>);
                    console.log("done");
                } catch (e) {
                    console.log(e);
                    setTimeout(reCallUntilItsWork, 1000);
                }
            }

            reCallUntilItsWork();
        </script>
        <?php
        return ob_get_clean();
    }
    , "map",
    [
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("latitude and longitude", "my-text-domain"),
            "param_name" => "lat-long",
            "value" => __("45.422224, -75.7420064", "my-text-domain")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("map id", "my-text-domain"),
            "param_name" => "mapid",
            "value" => __("map1", "my-text-domain")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("height", "my-text-domain"),
            "param_name" => "height",
            "value" => __("400px", "my-text-domain")
        ),
        array(
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => __("color map", "my-text-domain"),
            "param_name" => "color",
            "value" => "#333"
        ),
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("icon", "my-text-domain"),
            "param_name" => "icon",
            "value" => __("0", "my-text-domain")
        )
    ]);