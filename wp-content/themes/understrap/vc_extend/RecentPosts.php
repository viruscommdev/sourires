<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "recent_posts",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'nb_post' => '3',

        ), $atts);
        ob_start();


	    $args = array(
		    'numberposts' => $atts['n_post'],
		    'offset' => 0,
		    'category' => 0,
		    'orderby' => 'post_date',
		    'order' => 'DESC',
		    'include' => '',
		    'exclude' => '',
		    'meta_key' => '',
		    'meta_value' =>'',
		    'post_type' => 'post',
		    'post_status' => 'publish',
		    'suppress_filters' => true
	    );

	    $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

        ?>
        <div class="recent_posts row col-md">
            <?php foreach ( $recent_posts as $item ):?>
            <div class="recent_post col-md-<?php echo 12 / $atts['nb_post'] ?> col-sm<?php echo 12 / $atts['nb_post'] ?> col-xs-12">

                <div class="col-md-12 recent_post_content" style="background: url('<?php echo get_the_post_thumbnail_url($item['ID'],'full') ?>') no-repeat center #63ccca;">
                    <div class="recent_post_content-date">
                        <p class="day"><?php echo date("d", strtotime($item['post_date'])); ?></p>
                        <p class="month"><?php echo date("M", strtotime($item['post_date'])); ?></p>
                        <p class="year"><?php echo date("Y", strtotime($item['post_date'])); ?></p>
                    </div>

                    <div class="recent_post_content-footer">
                        <h3><?php echo $item["post_title"] ?></h3>
                        <img class="icon_" src="/wp-content/uploads/2018/10/Group-81.png">
                    </div>

                </div>

            </div>
        <?php endforeach; ?>
        </div>
        <?php
        return ob_get_clean();
    }
    , "recent posts",
    [

        [
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("recent posts number", "my-text-domain"),
            "param_name" => "nb_post",
            "value" => __("", "my-text-domain")
        ]

    ]);