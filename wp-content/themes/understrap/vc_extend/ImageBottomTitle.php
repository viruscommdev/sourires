<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "ImageBottomTitle",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'image' => '',
            'title' => '',
            'linkurl'=>'',
        ), $atts);
        ob_start();

        ?>
        <div class="ibt text-center">
<a href="<?php echo $atts['linkurl'] ?>">
                <div class="image"> <?php echo wp_get_attachment_image($atts['image'],"full"); ?></div>
                <h3 class="equalHeightsoins"><?php echo $atts['title'] ?></h3>
</a>
        </div>
        <?php
        return ob_get_clean();
    }
    , "ImageBottomTitle",
    [
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("titile", "my-text-domain"),
            "param_name" => "image",
            "value" => __("0", "my-text-domain")
        ),
        [
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("title", "my-text-domain"),
            "param_name" => "title",
            "value" => __("", "my-text-domain")
        ],
        [
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("url", "my-text-domain"),
            "param_name" => "linkurl",
            "value" => __("", "my-text-domain")
        ],

    ]);