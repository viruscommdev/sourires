<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "soin_dentaire_block",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'image' => '',
            'header' => '',
            'content-text'=>'',
            'btn'=>'',
            'link'=>''
        ), $atts);
        ob_start();

        ?>
        <div class="soind text-center">
            <div class="soind-c">
                <div class="image"> <?php echo wp_get_attachment_image($atts['image'],"full"); ?></div>
                <div class="content content-soind">
                    <h3 class="header"> <?php echo $atts['header']; ?></h3>
                    <p><?php echo $atts['content-text']; ?></p>
                   <a href="<?php echo  $atts['link']; ?>"> <button ><?php echo  $atts['btn']; ?></button> </a>
                </div>

            </div>
        </div>
        <?php
        return ob_get_clean();
    }
    , "soin dentaires block",
    [
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("image", "my-text-domain"),
            "param_name" => "image",
            "value" => __("0", "my-text-domain")
        ),
        [
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("header", "my-text-domain"),
            "param_name" => "header",
            "value" => __("", "my-text-domain")
        ],
	    [
		    "type" => "textarea",
		    "holder" => "div",
		    "class" => "",
		    "heading" => __("content-text", "my-text-domain"),
		    "param_name" => "content-text",
		    "value" => __("", "my-text-domain")
	    ],
	    [
		    "type" => "textfield",
		    "holder" => "div",
		    "class" => "",
		    "heading" => __("btn text", "my-text-domain"),
		    "param_name" => "btn",
		    "value" => __("", "my-text-domain")
	    ],
	    [
		    "type" => "textfield",
		    "holder" => "div",
		    "class" => "",
		    "heading" => __("btn link", "my-text-domain"),
		    "param_name" => "link",
		    "value" => __("", "my-text-domain")
	    ]
    ]);