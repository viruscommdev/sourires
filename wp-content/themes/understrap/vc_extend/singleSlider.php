<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
	"single_slider",
	function ($atts, $content = null) {
		$atts = shortcode_atts(array(
			'image' => '',
			'text_top' => '',
			'text_bottom'=>''
		), $atts);
		ob_start();

		?>

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="3000">
            <div class="carousel-inner" style="background: url('<?php echo wp_get_attachment_url($atts['image']); ?>') no-repeat no-repeat; background-size: cover">

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <div id="caption-slider">
                <h3><?php echo  $atts['text_top']?></h3>
                <h1><?php echo  $atts['text_bottom']?></h1>

            </div>

        </div>
		<?php
		return ob_get_clean();
	}
	, "single slider",
	[
		array(
			"type" => "attach_image",
			"holder" => "div",
			"class" => "",
			"heading" => __("image", "my-text-domain"),
			"param_name" => "image",
			"value" => __("0", "my-text-domain")
		),
		[
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("text top", "my-text-domain"),
			"param_name" => "text_top",
			"value" => __("", "my-text-domain")
		],
		[
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("text bottom", "my-text-domain"),
			"param_name" => "text_bottom",
			"value" => __("", "my-text-domain")
		],
	]);