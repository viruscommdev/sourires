<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 22/03/2018
 * Time: 11:54
 */
add_shortcode("date_post",function (){
    ?>
    <script>
        jQuery(".datepost").each(function () {
            var arr=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
           var x=jQuery(this).data("date");
           x=moment(x,"MMM DD, YYYY HH:mm",'fr')._d;
           jQuery(this).find(".day").text(x.getDate());
           jQuery(this).find(".month").text(arr[x.getMonth()]);
        });
    </script>
<?php
    return "<div class='datepost' data-date='{{ post_data:post_datetime }}' ><div class='day'></div><div class='month'></div></div>";
});
add_shortcode("tracking_post",function (){
   return '<ul id="tracking"><li><i class="fas fa-eye colorTheme"></i></li><li>[epvc_views]</li><li> <i class="fas fa-comment-alt colorTheme"></i></li><li> {{ post_data:comment_count }}</li></ul>';
});

add_shortcode("comment_post",function (){
    return "{{ post_data:comment_count }}";
});
?>
