<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "slider_with_content",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'image1' => '',
            'height' => '400px',
            'contentslider1'=>""
        ), $atts);
        ob_start();
        ?>
     <!--   <div class="carousel carousel-slider center"  style="    width:100%;">
            <div class="carousel-item red white-text" href="#one!">
                <?=wp_get_attachment_image($atts['image1'],"full") ?>
                <div class="contentSlider">
                    <?= urldecode(base64_decode($atts["contentslider1"])) ?>
                </div>
            </div>
           

        </div>
        <script>

        </script> -->
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="3000">
            <div class="carousel-inner">
                <div class="carousel-item text-right active">
                    <img class="model "  src="/wp-content/uploads/2018/09/Image-03@2x.png" alt="First slide">

                </div>
                <div class="carousel-item text-right ">
                    <img class="model "  src="/wp-content/uploads/2018/09/Image-05@2x.png" alt="First slide">

                </div>
                <div class="carousel-item text-right ">
                    <img class="model "  src="/wp-content/uploads/2018/11/slider3.png" alt="First slide">

                </div>
                <div class="carousel-item text-right ">
                    <img class="model "  src="/wp-content/uploads/2018/09/Image-02@2x.png" alt="First slide">

                </div>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <div id="caption-slider">
                <h3>Ma santé, mon sourire...</h3>
                <p class="slidermobilep" style="font-family: minimolight; font-size: 60px;line-height: 0.8;max-width: 350px;">Mon espace
                    sourire</p>


            </div>
            <div id="caption-slider-top">
                <img src="/wp-content/uploads/2018/10/Group-13@2x.png"><div class="text-center">Prendre un RENDEZ-VOUS<br> <a href="tel:4506880800" style="color: white;text-decoration: none;">450-688-0800</a></div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
    , "slider with content",
    [
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("image 1", "my-text-domain"),
            "param_name" => "image1",
            "value" => __("0", "my-text-domain")
        ),
        [
            "type" => "textarea_raw_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("content Slider 1", "my-text-domain"),
            "param_name" => "contentslider1",
            "value" => __("", "my-text-domain")
        ],
    ]);