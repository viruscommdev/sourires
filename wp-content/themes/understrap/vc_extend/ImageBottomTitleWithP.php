<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 11/10/2017
 * Time: 11:23
 */
$vc_module->all_in_one(
    "ImageBottomTitleWithP",
    function ($atts, $content = null) {
        $atts = shortcode_atts(array(
            'image' => '',
            'title' => '',
            'text'=> '',
        ), $atts);
        ob_start();

        ?>
        <div class="ibtp text-center">

                <div class="image"> <?php echo wp_get_attachment_image($atts['image'],"full"); ?></div>
                <h3 class="equalHeightequipep"><?php echo $atts['title'] ?></h3>
                <p><?php echo $atts['text'] ?></p>

        </div>
        <?php
        return ob_get_clean();
    }
    , "ImageBottomTitleWithP",
    [
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("titile", "my-text-domain"),
            "param_name" => "image",
            "value" => __("0", "my-text-domain")
        ),
        [
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("title", "my-text-domain"),
            "param_name" => "title",
            "value" => __("", "my-text-domain")
        ],
	    [
		    "type" => "textarea",
		    "holder" => "div",
		    "class" => "",
		    "heading" => __("text", "my-text-domain"),
		    "param_name" => "text",
		    "value" => __("", "my-text-domain")
	    ],

    ]);