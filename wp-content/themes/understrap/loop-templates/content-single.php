<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php
        if ('post' === get_post_type()) :
            ?>
            <div class="singleImgArticle">
                <div class="entry-meta">
                    <div class="dateArticle">
                        <div class="datecontent">
                            <div class="day font24"><?=get_the_date('d')?></div>
                            <div class="month font24"><?=get_the_date('M')?></div>
                            <div class="month font24"><?=get_the_date('Y')?></div>
                        </div>
                    </div>
                    <?= wp_get_attachment_image(get_post_thumbnail_id( ), "full") ?>
                </div>

                <div class="titleArticle">
                    <h1 class="font30">  <?=get_the_title()?> </h1>




                </div>
            </div><!-- .entry-meta -->
        <?php endif; ?>
    </header><!-- .entry-header -->


    <div class="entry-content">
        <?php
        the_content();

        ?>
    </div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->
