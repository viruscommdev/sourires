<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper col-md-10 width80" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info row">

                        <div class="col-md-4 f1">
                            <img width="273" height="45" src="/wp-content/uploads/2018/10/logo-espaces-sourires-footer.png" class="img-fluid" alt="les espaces sourires" alt="Espaces sourires logo-Footer">
                            <p> Aux Espaces Sourires, nous sommes 6 dentistes et une équipe de professionnels dentaires qualifiés qui offrent une gamme complète de soins dentaires dans nos cliniques de Laval.</p>
                            <p style="margin: 30px 0;"><a href="#"><img src="/wp-content/uploads/2018/10/facebook-rs.png" /> </a><a style="margin: 0 3px;" href="#"><img src="/wp-content/uploads/2018/10/twitter-rs.png" /> </a><a href="#"><img src="/wp-content/uploads/2018/10/youtube-rs.png" /> </a></p>
                        </div>
                        <div class="col-md-8 row">
                            <div class="col-md-4 f2">
                                <p class="head"><i class="fa fa-info"></i> INFORMATION</p>
                                <hr>
                                <p class="body">
                                    <ul class="menu-footer">
                                    <li><a href="/soins-dentaires-laval/implantologie/les-implants-dentaires/">À propos</a></li>
                                    <li><a href="/centre-dentaire-chomedey-laval/">Cliniques dentaires</a></li>
                                    <li><a href="/soins-dentaires-laval/">Soins Dentaires</a></li>
                                    <li><a href="/conseils-dentaires-postoperatoires/">Conseils postopératoires</a></li>
                                    <li><a href="/urgences-dentaires-laval-24-heures/">Urgences dentaires</a></li>
                                    <li><a href="/technologies/">Technologies</a></li>
                                    <li><a href="/blogue/">Blogue</a></li>
                                </ul>
                                </p>
                            </div>
                            <div class="col-md-5 f3">
                                <p class="head"><i class="fa fa-map-marker"></i> NOUS JOINDRE</p>
                                <hr>
                                <p class="body">
                                    Espace Sourire Chomedey<br>
                                    4670 boulevard St-Martin Ouest, suite 202<br>
                                    Laval, QC, H7T 2Y2<br>
                                    <a class="body" style="text-decoration: none;" href="tel:4506880800">450-688-0800</a><br><br>
                                    Espace Sourire St-François<br>
                                    8048, avenue Marcel-Villeneuve #203<br>
                                    Laval, QC, H7A 4H5<br>
                                    <a class="body" style="text-decoration: none;" href="tel:4509368686">450-936-8686</a>
                                </p>
                            </div>
                            <div class="col-md-3 f3">
                            </div>
                        </div>
					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

<?php wp_footer(); ?>
</div><!-- #page we need this extra closing tag here -->



</body>

</html>

