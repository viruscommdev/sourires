<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$post_12 = get_post(1097);
echo do_shortcode($post_12->post_content);
?>

<div class="wrapper col-md-10 width80" id="index-wrapper">
<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">
            <div class="vc_col-sm-8 ">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'single' ); ?>

						<?php //understrap_post_nav(); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						//comments_template();
					endif;
					?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		<!-- Do the right sidebar check -->
		<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>
            </div>
            <div class="vc_col-sm-4">
                <?php $post_side = get_post(1114);
                echo do_shortcode($post_side->post_content); ?>
            </div>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->
</div>
<?php get_footer(); ?>
