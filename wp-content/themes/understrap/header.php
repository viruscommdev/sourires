<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>

   <!-- <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script> -->
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

<div class="row">
	<!-- ******************* The Navbar Area ******************* -->
    <div class="col-md-2 width20  nav-container fixed-top ">
    <div class="nav-side-menu">
        <!-- Your site title as branding in the menu -->
	    <?php if ( ! has_custom_logo() ) { ?>

            <?php if ( is_front_page() && is_home() ) : ?>

                <h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

            <?php else : ?>

                <a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

            <?php endif; ?>


	    <?php } else { ?>

        <div class="brand">
		  <?php  the_custom_logo(); ?>
        </div>
	  <?php  } ?>
        <i class="fa fa-bars fa-2x toggle-btn collapsed" data-toggle="collapse" data-target="#menu-content"></i>
        <!-- end custom logo -->

      <!--  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"</i> -->
<!--
        <div class="menu-list">


			 <ul id="menu-content" class="menu-content collapse out">
				 <li>
					 <a href="#">
						 <i class="fa fa-dashboard fa-lg"></i> Dashboard
					 </a>
				 </li>

				 <li  data-toggle="collapse" data-target="#products" class="collapsed active">
					 <a href="#"><i class="fa fa-gift fa-lg"></i> UI Elements <span class="arrow"></span></a>
				 </li>
				 <ul class="sub-menu collapse" id="products">
					 <li class="active"><a href="#">CSS3 Animation</a></li>
					 <li><a href="#">General</a></li>
					 <li><a href="#">Buttons</a></li>
					 <li><a href="#">Tabs & Accordions</a></li>
					 <li><a href="#">Typography</a></li>
					 <li><a href="#">FontAwesome</a></li>
					 <li><a href="#">Slider</a></li>
					 <li><a href="#">Panels</a></li>
					 <li><a href="#">Widgets</a></li>
					 <li><a href="#">Bootstrap Model</a></li>
				 </ul>


				 <li data-toggle="collapse" data-target="#service" class="collapsed">
					 <a href="#"><i class="fa fa-globe fa-lg"></i> Services <span class="arrow"></span></a>
				 </li>
				 <ul class="sub-menu collapse" id="service">
					 <li>New Service 1</li>
					 <li>New Service 2</li>
					 <li>New Service 3</li>
				 </ul>


				 <li data-toggle="collapse" data-target="#new" class="collapsed">
					 <a href="#"><i class="fa fa-car fa-lg"></i> New <span class="arrow"></span></a>
				 </li>
				 <ul class="sub-menu collapse" id="new">
					 <li>New New 1</li>
					 <li>New New 2</li>
					 <li>New New 3</li>
				 </ul>


				 <li>
					 <a href="#">
						 <i class="fa fa-user fa-lg"></i> Profile
					 </a>
				 </li>

				 <li>
					 <a href="#">
						 <i class="fa fa-users fa-lg"></i> Users
					 </a>
				 </li>
			 </ul>
			  </div>
        -->
	        <?php wp_nav_menu(
		        array(
			        'theme_location'  => 'primary',
			        'container_class' => 'menu-list',
			        'container_id'    => '',
			        'menu_class'      => 'menu-content sidebar-menu collapse', // collapse out
			        'fallback_cb'     => '', //Understrap_WP_Bootstrap_Navwalker::fallback
			        'menu_id'         => 'menu-content',
			        'depth'           => 4,
			        'walker'          =>  new Nav_Footer_Walker(), //new Understrap_WP_Bootstrap_Navwalker(),
		        )
	        );
                ?>
                <div class="row menu_tel hide-mobile">
                    <img src="/wp-content/uploads/2018/10/Group-13@2x.png">
                    <p>Prendre un rendez-vous<br>
                        <a style="color: white;text-decoration: none;" href="tel:4506880800">T. 450-688-0800</a><br>
                        <a style="color: white;text-decoration: none;"  href="tel:4509368686">T. 450-936-8686</a><br>
                    </p>
                </div>
                <?php
	        get_search_form();

	        ?>

        <div class="row menu_rs hide-mobile">
        <p style="margin-top: 25px;">Suivez-nous sur:</p>
        <p style="margin-top: 10px;"><a href="#"><img src="/wp-content/uploads/2018/10/facebook-rs.png" /> </a><a style="margin: 0 3px;" href="#"><img src="/wp-content/uploads/2018/10/twitter-rs.png" /> </a><a href="#"><img src="/wp-content/uploads/2018/10/youtube-rs.png" /> </a></p>
        </div>
<script>
    (function($){
        $(document).ready(function(){
            $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).parent().siblings().removeClass('open');
                $(this).parent().toggleClass('open');
            });
        });
    })(jQuery);
</script>
        <p class="text-center" style="color:#fff;font-size: 14px" > Urgences <strong><a style="color:#fff;text-decoration: none" href="tel:4506880800">450-688-0800</a></strong></p>
        <div class="hide-mobile" style="height: 300px"></div>
    </div>
    </div>